package fr.matthieu.jeannin.mobile;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class ManageTest {

    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference, testReference;
    private HashMap<String,Test> listTest = new HashMap<>();

    public ManageTest(){
        this.firebaseDatabase = FirebaseDatabase.getInstance();
        this.databaseReference = firebaseDatabase.getReference();
        this.testReference = databaseReference.child("Test");
    }

    public void ajoutTest(Test test){
        try {
            listTest.put(String.valueOf(test.getIdTest()),test);
            sauvegarde();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void sauvegarde(){
        try{
            testReference.setValue(listTest);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public FirebaseDatabase getFirebaseDatabase() {
        return firebaseDatabase;
    }

    public void setFirebaseDatabase(FirebaseDatabase firebaseDatabase) {
        this.firebaseDatabase = firebaseDatabase;
    }

    public DatabaseReference getDatabaseReference() {
        return databaseReference;
    }

    public void setDatabaseReference(DatabaseReference databaseReference) {
        this.databaseReference = databaseReference;
    }

    public DatabaseReference getTestReference() {
        return testReference;
    }

    public void setTestReference(DatabaseReference testReference) {
        this.testReference = testReference;
    }

    public HashMap<String, Test> getListTest() {
        return listTest;
    }

    public void setListTest(HashMap<String, Test> listTest) {
        this.listTest = listTest;
    }
}
