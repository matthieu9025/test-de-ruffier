package fr.matthieu.jeannin.mobile;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Date;
import java.util.HashMap;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DetailPatientFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DetailPatientFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailPatientFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public DetailPatientFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DetailPatientFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DetailPatientFragment newInstance(String param1, String param2) {
        DetailPatientFragment fragment = new DetailPatientFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    private LinearLayout linearLayout;
    private ManagePatient managePatient = new ManagePatient();
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_detail_patient, container, false);
        firebaseDatabase=FirebaseDatabase.getInstance();
        databaseReference=firebaseDatabase.getReference();

        linearLayout = view.findViewById(R.id.linearLayoutTest);

        synchronisationPatient(new SynchronisationPatient() {
            @Override
            public void onCallBack(HashMap<String, Patient> patientList) {
                managePatient.setListPatient(patientList);
                SharedPreferences sharedPreferences=getActivity().getSharedPreferences(getString(R.string.sharePreference),getActivity().MODE_PRIVATE);
                if (sharedPreferences.contains("idPatient")) {
                    for (String key : managePatient.getListPatient().keySet()){
                        if (String.valueOf(managePatient.getListPatient().get(key).getIdPatient()).equalsIgnoreCase(sharedPreferences.getString("idPatient",""))) {
                            Patient patient = managePatient.getListPatient().get(key);
                            TextView textViewTitrePatient = getView().findViewById(R.id.textViewTitrePatient);
                            textViewTitrePatient.setText(patient.getNom()+ "\n"+patient.getPrenom());
                            if(patient.getListeTest()!=null){
                                for (Test test : patient.getListeTest()) {
                                    TextView textView = new TextView(getContext());
                                    textView.setBackground(getContext().getDrawable(R.drawable.rectangle_test));
                                    textView.setText(
                                            "\n \nTest du " + test.getDate() + "\n"
                                                    + "Ir = " + test.getiR() + "\n"
                                                    + "Id = " + test.getiD() + "\n"
                                                    + "P0 = " + test.getP0() + " bpm\n"
                                                    + "P1 = " + test.getP1() + " bpm\n"
                                                    + "P2 = " + test.getP2() + " bpm\n"
                                    );
                                    textView.setGravity(Gravity.CENTER);
                                    linearLayout.addView(textView);
                                }
                            }


                            /*Test test = new Test(1, 2, 3, 4, 5, 5, new Date().toString());
                            TextView textView = new TextView(getContext());
                            textView.setBackground(getContext().getDrawable(R.drawable.rectangle_test));
                            textView.setText(
                                    "Test du " + test.getDate() + "\n"
                                            + "Ir = " + test.getiR() + "\n"
                                            + "Id = " + test.getiD() + "\n"
                                            + "P0 = " + test.getP0() + " bpm\n"
                                            + "P1 = " + test.getP1() + " bpm\n"
                                            + "P2 = " + test.getP2() + " bpm\n"
                            );
                            linearLayout.addView(textView);*/
                        }
                    }
                }
            }
        });

        Button buttonLaunchTest = view.findViewById(R.id.button3);
        buttonLaunchTest.setOnClickListener(clickLaunchTest);

        return view;
    }

    private View.OnClickListener clickLaunchTest = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            SynchronizeAsyncTask mSynchronizeAsyncTask = new SynchronizeAsyncTask(getActivity());
            mSynchronizeAsyncTask.execute(2);
            Log.d(TAG,"ici");
            FragmentManager fm = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(R.id.frameLayout,new TestFragment());
            fragmentTransaction.addToBackStack(null).commit();
        }
    };


    private void synchronisationPatient(final SynchronisationPatient Sync) {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                HashMap<String, Patient> patientList = new HashMap<>();

                for (DataSnapshot dataPatient : dataSnapshot.child("Patient").getChildren()) {
                    Patient patient = dataPatient.getValue(Patient.class);
                    patientList.put(String.valueOf(patient.getIdPatient()), patient);
                }
                Sync.onCallBack(patientList);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
