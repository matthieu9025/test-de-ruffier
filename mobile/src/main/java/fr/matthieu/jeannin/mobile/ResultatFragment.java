package fr.matthieu.jeannin.mobile;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ResultatFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ResultatFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ResultatFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ResultatFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ResultatFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ResultatFragment newInstance(String param1, String param2) {
        ResultatFragment fragment = new ResultatFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    private FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
    private DatabaseReference databaseReference = firebaseDatabase.getReference();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View root = inflater.inflate(R.layout.fragment_resultat, container, false);
        final ManagePatient managePatient = new ManagePatient();
        synchronisationPatient(new SynchronisationPatient() {
            @Override
            public void onCallBack(HashMap<String, Patient> patientList) {
                managePatient.setListPatient(patientList);
                SharedPreferences sharedPreferences = getActivity().getSharedPreferences(getString(R.string.sharePreference), getActivity().MODE_PRIVATE);
                if (sharedPreferences.contains("idPatient")) {
                    for (String key : managePatient.getListPatient().keySet()) {
                        if (String.valueOf(managePatient.getListPatient().get(key).getIdPatient()).equalsIgnoreCase(sharedPreferences.getString("idPatient", ""))) {
                            Patient patient = managePatient.getListPatient().get(key);
                            Test test = patient.getListeTest().get(patient.getListeTest().size()-1);
                            TextView textView = root.findViewById(R.id.textViewResultat);
                            textView.setText(test.toString());
                        }
                    }
                }
            }
        });
        Button button = root.findViewById(R.id.button5);
        button.setOnClickListener(retour);
        return root;
    }

    private View.OnClickListener retour = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            FragmentManager fm = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(R.id.frameLayout,new DetailPatientFragment());
            fragmentTransaction.commit();
        }

    };

            private void synchronisationPatient(final SynchronisationPatient Sync) {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                HashMap<String, Patient> patientList = new HashMap<>();

                for (DataSnapshot dataPatient : dataSnapshot.child("Patient").getChildren()) {
                    Patient patient = dataPatient.getValue(Patient.class);
                    patientList.put(String.valueOf(patient.getIdPatient()), patient);
                }
                Sync.onCallBack(patientList);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
