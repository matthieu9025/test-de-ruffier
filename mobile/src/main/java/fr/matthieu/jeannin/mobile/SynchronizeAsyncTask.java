package fr.matthieu.jeannin.mobile;

import android.app.Activity;
import android.os.AsyncTask;
import android.provider.SyncStateContract;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.wearable.DataClient;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

import static com.google.android.gms.wearable.Wearable.getDataClient;


public class SynchronizeAsyncTask extends AsyncTask<Integer, Integer, Integer> {
    private DataClient mDataClient ;
    public SynchronizeAsyncTask(Activity mContext) {
        super();
        mDataClient = getDataClient(mContext);
    }
    @Override
    protected Integer doInBackground(Integer... params) {
        Integer action = params[0];
        PutDataMapRequest putDataMapReq = PutDataMapRequest.create("/mobileActions");
        putDataMapReq.getDataMap().putInt("action" ,action);
        PutDataRequest putDataReq = putDataMapReq.asPutDataRequest();
        putDataReq.setUrgent();
        Task<DataItem> putDataTask = mDataClient.putDataItem(putDataReq);
        return -1;
    }
}