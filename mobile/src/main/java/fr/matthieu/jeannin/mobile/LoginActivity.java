package fr.matthieu.jeannin.mobile;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class LoginActivity extends AppCompatActivity {

    FirebaseAuth mAuth;
    private ManageMedecin manageMedecin = new ManageMedecin();
    private ManagePatient managePatient = new ManagePatient();
    private ManageTest manageTest = new ManageTest();
    private FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
    private DatabaseReference databaseReference = firebaseDatabase.getReference();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        synchronisationMedecin(new SynchronisationMedecin() {
            @Override
            public void onCallBack(HashMap<String, Medecin> medecinList) {
                synchronisationPatient(new SynchronisationPatient() {
                    @Override
                    public void onCallBack(HashMap<String, Patient> patientList) {
                        synchronisationTest(new SynchronisationTest() {
                            @Override
                            public void onCallBack(HashMap<String, Test> testList) {
                                mAuth = FirebaseAuth.getInstance();
                                Button buttonLogin = findViewById(R.id.buttonLogin);
                                buttonLogin.setOnClickListener(clickLogin);
                                TextView buttonRegister = findViewById(R.id.buttonRegister);
                                buttonRegister.setOnClickListener(clickRegister);
                            }
                        });

                    }
                });
            }
        });
    }

    private View.OnClickListener clickLogin = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            EditText editTextEmail = findViewById(R.id.editTextEmail);
            EditText editTextPassword = findViewById(R.id.editTextPassword);
            logerUser(editTextEmail.getText().toString(),editTextPassword.getText().toString());
        }
    };

    public void logerUser(final String email, String password){
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            //Toast.makeText(MainActivity.this,"Bienvenue"+email,Toast.LENGTH_LONG).show();
                            SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.sharePreference),MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("email",email);
                            editor.apply();
                            editor.commit();
                            Intent intentLogin = new Intent(getApplicationContext(),MainActivity.class);
                            startActivity(intentLogin);
                            finish();
                        } else {
                            if(task.getException() instanceof FirebaseAuthUserCollisionException){
                                Toast.makeText(getApplication(),"User Existent",Toast.LENGTH_LONG).show();
                            }else {
                                // If sign in fails, display a message to the user.
                                Toast.makeText(getApplication(),"ERROR Log",Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });
    }

    private View.OnClickListener clickRegister = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Test test = new Test(1, 2, 3, 4, 5, 5, new Date().toString());

            ArrayList<Test> arrayListTest = new ArrayList<>();
            arrayListTest.add(test);

            Patient patient = new Patient(managePatient.getListPatient().size()+1,"Benjamin","Burger",arrayListTest);
            manageTest.ajoutTest(test);
            managePatient.ajoutPatient(patient);

            Patient patient2 = new Patient(managePatient.getListPatient().size()+1,"Thomas","Verdun",new ArrayList<Test>());
            managePatient.ajoutPatient(patient2);


            Patient patient3 = new Patient(managePatient.getListPatient().size()+1,"Sam","Caïd",new ArrayList<Test>());
            managePatient.ajoutPatient(patient3);

            Patient patient4 = new Patient(managePatient.getListPatient().size()+1,"Castor","Pierre",new ArrayList<Test>());
            managePatient.ajoutPatient(patient4);

            Patient patient5 = new Patient(managePatient.getListPatient().size()+1,"LaGrande","Marion", new ArrayList<Test>());
            managePatient.ajoutPatient(patient5);

            ArrayList<Patient> listPatientMedecin1 = new ArrayList<>();
            listPatientMedecin1.add(patient);
            listPatientMedecin1.add(patient4);
            listPatientMedecin1.add(patient5);

            ArrayList<Patient> listPatientMedecin2 = new ArrayList<>();
            listPatientMedecin2.add(patient2);
            listPatientMedecin2.add(patient3);

            Medecin medecin = new Medecin(manageMedecin.getListMedecin().size()+1,"Lucas","leVrai", "LucasVrai@gmail.com","Mdp123456",listPatientMedecin1);
            manageMedecin.createMedecin(medecin);
            registrerUser("LucasVrai@gmail.com","Mdp123456");

            Medecin medecin2 = new Medecin(manageMedecin.getListMedecin().size()+1,"Pierre","Meyer","pierreMeyer@gmail.com","Mdp123456",listPatientMedecin2);
            manageMedecin.createMedecin(medecin2);
            manageMedecin.ajouterPatient(medecin2,patient5);
            managePatient.ajouterTest(patient,test);
            manageTest.ajoutTest(test);
            registrerUser("pierreMeyer@gmail.com","Mdp123456");

        }
    };

    public void registrerUser(final String email, final String password) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {


                    }
                }
                });
    }

    private void synchronisationMedecin(final SynchronisationMedecin Sync) {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                HashMap<String, Medecin> medecinList = new HashMap<>();

                for (DataSnapshot dataMedecin : dataSnapshot.child("Medecin").getChildren()) {
                    Medecin medecin = dataMedecin.getValue(Medecin.class);
                    medecinList.put(String.valueOf(medecin.getIdMedecin()), medecin);
                }
                Sync.onCallBack(medecinList);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void synchronisationTest(final SynchronisationTest Sync) {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                HashMap<String, Test> testList = new HashMap<>();

                for (DataSnapshot dataTest : dataSnapshot.child("Test").getChildren()) {
                    Test test = dataTest.getValue(Test.class);
                    testList.put(String.valueOf(test.getIdTest()), test);
                }
                Sync.onCallBack(testList);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void synchronisationPatient(final SynchronisationPatient Sync) {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                HashMap<String, Patient> patientList = new HashMap<>();

                for (DataSnapshot dataPatient : dataSnapshot.child("Patient").getChildren()) {
                    Patient patient = dataPatient.getValue(Patient.class);
                    patientList.put(String.valueOf(patient.getIdPatient()), patient);
                }
                Sync.onCallBack(patientList);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}
