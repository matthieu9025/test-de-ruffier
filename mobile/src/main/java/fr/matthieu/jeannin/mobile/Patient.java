package fr.matthieu.jeannin.mobile;

import java.util.ArrayList;

public class Patient {

    private int idPatient;
    private String nom,prenom;
    private ArrayList<Test> listeTest;

    public Patient(int idPatient, String nom, String prenom, ArrayList<Test> listeTest) {
        this.idPatient = idPatient;
        this.nom = nom;
        this.prenom = prenom;
        this.listeTest = listeTest;
    }

    public Patient() {
    }

    public int getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(int idPatient) {
        this.idPatient = idPatient;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public ArrayList<Test> getListeTest() {
        return listeTest;
    }

    public void setListeTest(ArrayList<Test> listeTest) {
        this.listeTest = listeTest;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "idPatient=" + idPatient +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", listeTest=" + listeTest +
                '}';
    }
}
