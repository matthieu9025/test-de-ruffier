package fr.matthieu.jeannin.mobile;

import java.util.HashMap;

public interface SynchronisationMedecin {

    void onCallBack(HashMap<String,Medecin> medecinList);
}
