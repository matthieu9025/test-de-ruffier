package fr.matthieu.jeannin.mobile;

import java.util.HashMap;

public interface SynchronisationTest {

    void onCallBack(HashMap<String,Test> testList);

}
