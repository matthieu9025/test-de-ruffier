package fr.matthieu.jeannin.mobile;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class ManagePatient {

    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference, patientReference;
    private HashMap<String,Patient> listPatient = new HashMap<>();

    public ManagePatient(){
        this.firebaseDatabase = FirebaseDatabase.getInstance();
        this.databaseReference = firebaseDatabase.getReference();
        this.patientReference = databaseReference.child("Patient");
    }

    //Create patient
    public void ajoutPatient(Patient patient){
        try {
            listPatient.put(String.valueOf(patient.getIdPatient()),patient);
            sauvegarde();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void sauvegarde(){
        try{
            patientReference.setValue(listPatient);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void ajouterTest(Patient patient, Test test){
        try{
            listPatient.get(patient).getListeTest().add(test);
            //listPatient.put(String.valueOf(listPatient.get(patient.getIdPatient())), patient);
            sauvegarde();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    //Ajout test

    public FirebaseDatabase getFirebaseDatabase() {
        return firebaseDatabase;
    }

    public void setFirebaseDatabase(FirebaseDatabase firebaseDatabase) {
        this.firebaseDatabase = firebaseDatabase;
    }

    public DatabaseReference getDatabaseReference() {
        return databaseReference;
    }

    public void setDatabaseReference(DatabaseReference databaseReference) {
        this.databaseReference = databaseReference;
    }

    public DatabaseReference getPatientReference() {
        return patientReference;
    }

    public void setPatientReference(DatabaseReference patientReference) {
        this.patientReference = patientReference;
    }

    public HashMap<String, Patient> getListPatient() {
        return listPatient;
    }

    public void setListPatient(HashMap<String, Patient> listPatient) {
        this.listPatient = listPatient;
    }
}
