package fr.matthieu.jeannin.mobile;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TooManyListenersException;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PatientFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PatientFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PatientFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public PatientFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PatientFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PatientFragment newInstance(String param1, String param2) {
        PatientFragment fragment = new PatientFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    private LinearLayout linearLayout;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private ManagePatient managePatient = new ManagePatient();
    private ManageMedecin manageMedecin = new ManageMedecin();
    private SharedPreferences sharedPreferences;
    private int idButton= 1;
    private Patient patient;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        firebaseDatabase=FirebaseDatabase.getInstance();
        databaseReference=firebaseDatabase.getReference();

        View view = inflater.inflate(R.layout.fragment_patient, container, false);

        linearLayout = view.findViewById(R.id.linearLayoutBouton);
        sharedPreferences=getActivity().getSharedPreferences(getString(R.string.sharePreference),getActivity().MODE_PRIVATE);;
        synchronisationMedecin(new SynchronisationMedecin() {
            @Override
            public void onCallBack(HashMap<String, Medecin> medecinList) {
                manageMedecin.setListMedecin(medecinList);
                if (sharedPreferences.contains("email")) {
                    for (String key : manageMedecin.getListMedecin().keySet()){
                        if (manageMedecin.getListMedecin().get(key).geteMail().equalsIgnoreCase(sharedPreferences.getString("email",""))){
                            Medecin medecin = manageMedecin.getListMedecin().get(key);
                            for (Patient patient : medecin.getListePatient()){
                                Button button = new Button(getContext());
                                button.setText(patient.getNom() +" "+patient.getPrenom());
                                button.setBackground(getContext().getDrawable(R.drawable.rectangle_patient_bouton));
                                button.setId(patient.getIdPatient());
                                button.setOnClickListener(clickOnButton);
                                linearLayout.addView(button);
                            }
                        }
                    }
                }
            }
        });
        return  view;
    }

    private View.OnClickListener clickOnButton = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("idPatient",String.valueOf(v.getId()));
            editor.apply();
            editor.commit();

            FragmentManager fm = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(R.id.frameLayout,new DetailPatientFragment());
            fragmentTransaction.addToBackStack(null).commit();
        }
    };


    private void synchronisationMedecin(final SynchronisationMedecin Sync) {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                HashMap<String, Medecin> medecinList = new HashMap<>();

                for (DataSnapshot dataMedecin : dataSnapshot.child("Medecin").getChildren()) {
                    Medecin medecin = dataMedecin.getValue(Medecin.class);
                    medecinList.put(String.valueOf(medecin.getIdMedecin()), medecin);
                }
                Sync.onCallBack(medecinList);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}



