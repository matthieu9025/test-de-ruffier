package fr.matthieu.jeannin.mobile;

import java.util.HashMap;

public interface SynchronisationPatient {

    void onCallBack(HashMap<String,Patient> patientList);

}
