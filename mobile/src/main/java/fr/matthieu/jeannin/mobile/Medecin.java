package fr.matthieu.jeannin.mobile;

import java.util.ArrayList;

public class Medecin {

    private int idMedecin;
    private String prenom,nom,eMail,mdp;
    private ArrayList<Patient> listePatient;

    public Medecin(int idMedecin, String prenom, String nom, String eMail, String mdp, ArrayList<Patient> listePatient) {
        this.idMedecin = idMedecin;
        this.prenom = prenom;
        this.nom = nom;
        this.eMail = eMail;
        this.mdp = mdp;
        this.listePatient = listePatient;
    }

    public Medecin() {
    }

    public int getIdMedecin() {
        return idMedecin;
    }

    public void setIdMedecin(int idMedecin) {
        this.idMedecin = idMedecin;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    public ArrayList<Patient> getListePatient() {
        return listePatient;
    }

    public void setListePatient(ArrayList<Patient> listePatient) {
        this.listePatient = listePatient;
    }

    @Override
    public String toString() {
        return "Medecin{" +
                "idMedecin=" + idMedecin +
                ", prenom='" + prenom + '\'' +
                ", nom='" + nom + '\'' +
                ", eMail='" + eMail + '\'' +
                ", mdp='" + mdp + '\'' +
                ", listePatient=" + listePatient +
                '}';
    }
}
