package fr.matthieu.jeannin.mobile;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class ManageMedecin {

    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference,medecinReference;
    private HashMap<String,Medecin> listMedecin = new HashMap<>();

    public ManageMedecin(){
        this.firebaseDatabase = FirebaseDatabase.getInstance();
        this.databaseReference = firebaseDatabase.getReference();
        this.medecinReference = databaseReference.child("Medecin");
    }

    public void createMedecin(Medecin medecin){
        try{
            this.listMedecin.put(String.valueOf(medecin.getIdMedecin()),medecin);
            sauvegarde();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void ajouterPatient(Medecin medecin, Patient patient){
        try{
            listMedecin.get(medecin).getListePatient().add(patient);
           // listMedecin.put(String.valueOf(listMedecin.get(medecin.getIdMedecin())),medecin);
            sauvegarde();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void sauvegarde(){
        try{
            medecinReference.setValue(listMedecin);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public FirebaseDatabase getFirebaseDatabase() {
        return firebaseDatabase;
    }

    public void setFirebaseDatabase(FirebaseDatabase firebaseDatabase) {
        this.firebaseDatabase = firebaseDatabase;
    }

    public DatabaseReference getDatabaseReference() {
        return databaseReference;
    }

    public void setDatabaseReference(DatabaseReference databaseReference) {
        this.databaseReference = databaseReference;
    }

    public DatabaseReference getMedecinReference() {
        return medecinReference;
    }

    public void setMedecinReference(DatabaseReference medecinReference) {
        this.medecinReference = medecinReference;
    }

    public HashMap<String, Medecin> getListMedecin() {
        return listMedecin;
    }

    public void setListMedecin(HashMap<String, Medecin> listMedecin) {
        this.listMedecin = listMedecin;
    }
}
