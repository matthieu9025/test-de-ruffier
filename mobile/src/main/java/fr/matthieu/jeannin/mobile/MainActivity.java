package fr.matthieu.jeannin.mobile;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.wearable.DataClient;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.Wearable;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Date;
import java.util.HashMap;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class MainActivity extends AppCompatActivity implements DataClient.OnDataChangedListener,PatientFragment.OnFragmentInteractionListener, DetailPatientFragment.OnFragmentInteractionListener,TestFragment.OnFragmentInteractionListener, ResultatFragment.OnFragmentInteractionListener {

    private FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
    private DatabaseReference databaseReference = firebaseDatabase.getReference();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(R.id.frameLayout,new PatientFragment());
            fragmentTransaction.commit();
        }else
        {
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(R.id.frameLayout,new PatientFragment());
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        Log.d(TAG, "onDataChanged" + dataEvents);
        for (DataEvent event : dataEvents) {
            if (event.getType() == DataEvent.TYPE_CHANGED) {
// DataItem changed
                DataItem item = event.getDataItem();
                if (item.getUri().getPath().compareTo("/count") == 0) {
                    DataMap dataMap = DataMapItem.fromDataItem(item).getDataMap();
                    int hr = dataMap.getInt("pouls", 0);
                    try {
                        if (hr == 1) {
                            ConstraintLayout cl = findViewById(R.id.hrLog);
                            cl.setBackgroundColor(Color.parseColor("#192a56"));
                            TextView tv = findViewById(R.id.titleHRTV);
                            tv.setText("Reposez-vous");
                            TextView mTextView = findViewById(R.id.stepsTV);
                            mTextView.setText("");
                        } else if (hr == 2) {
                            ConstraintLayout cl = findViewById(R.id.hrLog);
                            cl.setBackgroundColor(Color.parseColor("#fbc531"));
                            TextView tv = findViewById(R.id.titleHRTV);
                            tv.setText("faites vos squats");
                        } else if (hr == 3) {
                            ConstraintLayout cl = findViewById(R.id.hrLog);
                            cl.setBackgroundColor(Color.parseColor("#192a56"));
                            TextView tv = findViewById(R.id.titleHRTV);
                            tv.setText("Reposez-vous");
                            TextView mTextView = findViewById(R.id.stepsTV);
                            mTextView.setText("");
                        } else if (hr == 4) {
                            FragmentManager fm = this.getSupportFragmentManager();
                            FragmentTransaction fragmentTransaction = fm.beginTransaction();
                            fragmentTransaction.replace(R.id.frameLayout,new ResultatFragment());
                            fragmentTransaction.addToBackStack(null).commit();
                        } else {
                            TextView tv = findViewById(R.id.hrTV);
                            tv.setText(hr + " bmp");
                        }


                    } catch (Exception e) {
                        Log.w(TAG, e);

                    }
                }


                if (item.getUri().getPath().compareTo("/time") == 0) {
                    DataMap dataMap = DataMapItem.fromDataItem(item).getDataMap();
                    int millisUntilFinished = dataMap.getInt("timer", 0);
                    try {
                        int minutes = (int) ((millisUntilFinished / 1000)/60);
                        int seconds = (int) ((millisUntilFinished /1000)%60);
                        int millis = (int) ((millisUntilFinished % 1000)/10);
                        TextView mTextView = findViewById(R.id.hrTimeTV);

                        mTextView.setText("" + minutes + ":" + seconds + ":" + millis );


                    } catch (Exception e) {
                        Log.w(TAG, e);

                    }
                }
                if (item.getUri().getPath().compareTo("/steps") == 0) {
                    DataMap dataMap = DataMapItem.fromDataItem(item).getDataMap();
                    int stepsNb = dataMap.getInt("steps", 0);
                    try {

                        TextView mTextView = findViewById(R.id.stepsTV);

                        mTextView.setText(stepsNb + " squats");


                    } catch (Exception e) {
                        Log.w(TAG, e);

                    }
                }

                if (item.getUri().getPath().compareTo("/result") == 0) {
                    DataMap dataMap = DataMapItem.fromDataItem(item).getDataMap();
                    String stepsNb = dataMap.getString("results");
                    try {
                        final String[] strSplit = stepsNb.split(";");
                        final int p0 = Integer.parseInt(strSplit[0]);
                        final int p1 = Integer.parseInt(strSplit[1]);
                        final int p2 = Integer.parseInt(strSplit[2]);
                        final double ir = (p0+p1+p2-200)/10;
                        final double id = (p1+2*(p2-p0))/10;
                        final ManagePatient managePatient = new ManagePatient();
                        final ManageTest manageTest = new ManageTest();
                        synchronisationPatient(new SynchronisationPatient() {
                            @Override
                            public void onCallBack(HashMap<String, Patient> patientList) {
                                managePatient.setListPatient(patientList);
                                synchronisationTest(new SynchronisationTest() {
                                    @Override
                                    public void onCallBack(HashMap<String, Test> testList) {
                                        manageTest.setListTest(testList);
                                        SharedPreferences sharedPreferences=getSharedPreferences(getString(R.string.sharePreference),MODE_PRIVATE);
                                        if (sharedPreferences.contains("idPatient")) {
                                            for (String key : managePatient.getListPatient().keySet()) {
                                                if (String.valueOf(managePatient.getListPatient().get(key).getIdPatient()).equalsIgnoreCase(sharedPreferences.getString("idPatient", ""))) {
                                                    Patient patient = managePatient.getListPatient().get(key);
                                                    Test test = new Test(testList.size(),p0,p1,p2,ir,id, new Date().toString());
                                                    managePatient.ajouterTest(patient,test);
                                                    manageTest.ajoutTest(test);
                                                }
                                            }
                                        }
                                    }
                                });
                            }
                        });

                    } catch (Exception e) {
                        Log.w(TAG, e);

                    }
                }

            }
        }

    }

    private void synchronisationPatient(final SynchronisationPatient Sync) {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                HashMap<String, Patient> patientList = new HashMap<>();

                for (DataSnapshot dataPatient : dataSnapshot.child("Patient").getChildren()) {
                    Patient patient = dataPatient.getValue(Patient.class);
                    patientList.put(String.valueOf(patient.getIdPatient()), patient);
                }
                Sync.onCallBack(patientList);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void synchronisationTest(final SynchronisationTest Sync) {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                HashMap<String, Test> testList = new HashMap<>();

                for (DataSnapshot dataTest : dataSnapshot.child("Test").getChildren()) {
                    Test test = dataTest.getValue(Test.class);
                    testList.put(String.valueOf(test.getIdTest()), test);
                }
                Sync.onCallBack(testList);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Wearable.getDataClient(this).addListener(this);
    }
    @Override
    protected void onPause() {
        super.onPause();
        Wearable.getDataClient(this).removeListener(this);
    }


}
