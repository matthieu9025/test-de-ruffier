package fr.matthieu.jeannin.mobile;

public class Test {

    private int idTest;
    private int p0,p1,p2;
    private double iR, iD;
    private String date;

    public Test(int idTest, int p0, int p1, int p2, double iR, double iD, String date) {
        this.idTest = idTest;
        this.p0 = p0;
        this.p1 = p1;
        this.p2 = p2;
        this.iR = iR;
        this.iD = iD;
        this.date = date;
    }

    public Test() {
    }

    public int getIdTest() {
        return idTest;
    }

    public void setIdTest(int idTest) {
        this.idTest = idTest;
    }

    public int getP0() {
        return p0;
    }

    public void setP0(int p0) {
        this.p0 = p0;
    }

    public int getP1() {
        return p1;
    }

    public void setP1(int p1) {
        this.p1 = p1;
    }

    public int getP2() {
        return p2;
    }

    public void setP2(int p2) {
        this.p2 = p2;
    }

    public double getiR() {
        return iR;
    }

    public void setiR(int iR) {
        this.iR = iR;
    }

    public double getiD() {
        return iD;
    }

    public void setiD(int iD) {
        this.iD = iD;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
