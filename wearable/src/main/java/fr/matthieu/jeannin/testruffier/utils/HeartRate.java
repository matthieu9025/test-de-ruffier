package fr.matthieu.jeannin.testruffier.utils;

public class HeartRate {
    private int heartRate = 0;

    public void updateHeartRate(int hr){
        heartRate = hr;
    }

    public int getHeartRate() {
        return heartRate;
    }
}
