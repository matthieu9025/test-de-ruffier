package fr.matthieu.jeannin.testruffier;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import fr.matthieu.jeannin.testruffier.utils.HeartRate;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class WearHRService extends Service implements SensorEventListener {


    public static final String HEART_RATE_MESSAGE = "mesure";
    public static final String HEART_RATE_VALUE = "pouls";
    private SensorManager sensorManager;
    private Sensor hrSensor;
    public HeartRate hr = new HeartRate();

    @Override
    public void onSensorChanged(SensorEvent event) {
        Log.d(TAG, "onSensorChanged");
        if (event.sensor.getType() == Sensor.TYPE_HEART_RATE) {
            hr.updateHeartRate((int)event.values[0]);
            sendHRUpdate();
        }
    }
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        Log.d(TAG, "onAccuracyChanged");
        sendHRUpdate();
    }
    private void sendHRUpdate() {
        Intent intent = new Intent();
        intent.setAction(HEART_RATE_MESSAGE);
        intent.putExtra(HEART_RATE_VALUE,hr.getHeartRate());
        sendBroadcast(intent);
    }

    

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStart");
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        hrSensor = sensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE);
        Log.d(TAG, hrSensor.getName());
        sensorManager.registerListener(this, hrSensor, SensorManager.SENSOR_DELAY_NORMAL);
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
