package fr.matthieu.jeannin.testruffier;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.wearable.activity.WearableActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.android.gms.wearable.DataClient;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.Wearable;

import java.io.IOException;
import java.util.List;

import androidx.core.app.ActivityCompat;
;

import fr.matthieu.jeannin.testruffier.utils.HeartRate;
import fr.matthieu.jeannin.testruffier.utils.SynchronizeAsyncTask;
import fr.matthieu.jeannin.testruffier.utils.SynchronizeAsyncTaskResults;
import fr.matthieu.jeannin.testruffier.utils.SynchronizeAsyncTaskSteps;
import fr.matthieu.jeannin.testruffier.utils.SynchronizeAsyncTaskTimer;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class MainActivity extends WearableActivity implements DataClient.OnDataChangedListener {



    public class MyReceiver extends BroadcastReceiver {
        public static final String ACTION_RESP ="com.myapp.intent.action.TEXT_TO_DISPLAY";
        private Activity activity;

        public MyReceiver(Activity activity) {
            this.activity = activity;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            try{
                int hrate = intent.getIntExtra(WearHRService.HEART_RATE_VALUE,0);
                // send text to display
                TextView result = (TextView) findViewById(R.id.hrTV);
                SynchronizeAsyncTask mSynchronizeAsyncTask = new SynchronizeAsyncTask(activity);
                mSynchronizeAsyncTask.execute(hrate);
                Log.d(TAG, "receive");

                result.setText("" + hrate);
            }catch (Exception e){
                Log.w(TAG, e);

            }

        }
    }

    private MyReceiver receiver;

    private int hrReposP0;
    private int hrQuatsP1;
    private int hrRecupP2;

    private Activity activity = this;
    int tempsRepos1 = 60000;//180000
    int tempsSquats = 45000;//45000
    int tempsReposFinal = 60000;//60000


    //private MyReceiver receiver;
    private boolean mWearBodySensorsPermissionApproved;
    private static final int PERMISSION_REQUEST_READ_BODY_SENSORS = 1;

    //DatabaseReference refMessage;

    private Intent hrServiceIntent;

    public HeartRate hr;

    //couleurs choisies pour l'app : https://flatuicolors.com/palette/gb
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        if (mWearBodySensorsPermissionApproved) {

            // To keep the sample simple, we are only displaying the number of sensors.
            SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
            List<Sensor> sensorList = sensorManager.getSensorList(Sensor.TYPE_ALL);
            int numberOfSensorsOnDevice = sensorList.size();

            //logToUi(numberOfSensorsOnDevice + " sensors on device(s)!");

        }else {
            //logToUi("Requested local permission.");
            // On 23+ (M+) devices, GPS permission not granted. Request permission.
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.BODY_SENSORS},
                    PERMISSION_REQUEST_READ_BODY_SENSORS);
        }
        setContentView(R.layout.activity_main);
        receiver = new MyReceiver(this);
        registerReceiver(receiver,new IntentFilter(WearHRService.HEART_RATE_MESSAGE));

        //FirebaseDatabase database = FirebaseDatabase.getInstance();
        //DatabaseReference myRef = database.getReference();
        //refMessage = myRef.child("Medecin");


        // Enables Always-on
        setAmbientEnabled();
        hr = new HeartRate();
        hrServiceIntent = new Intent(this, WearHRService.class);
        startService(hrServiceIntent);
        /*receiver = new MyReceiver();
        IntentFilter filter = new IntentFilter();*/
        //registerReceiver(receiver, filter);


        findViewById(R.id.startButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               poulsRepos();
            }
        });
    }


    public void poulsRepos(){
       setContentView(R.layout.pouls_repos_layout);
        SynchronizeAsyncTask mSynchronizeAsyncTask = new SynchronizeAsyncTask(this);
        mSynchronizeAsyncTask.execute(1);


       final TextView mTextView = findViewById(R.id.timerReposTV);

        new CountDownTimer(tempsRepos1, 100) {

            public void onTick(long millisUntilFinished) {
                SynchronizeAsyncTaskTimer mSynchronizeAsyncTask = new SynchronizeAsyncTaskTimer(activity);
                mSynchronizeAsyncTask.execute((int) millisUntilFinished);
                int minutes = (int) ((millisUntilFinished / 1000)/60);
                int seconds = (int) ((millisUntilFinished /1000)%60);
                int millis = (int) ((millisUntilFinished % 1000)/10);

                mTextView.setText("" + minutes + ":" + seconds + ":" + millis );
            }

            public void onFinish() {
                TextView result = (TextView) findViewById(R.id.hrTV);
                mTextView.setText(result.getText() + " bpm");
                hrReposP0 = Integer.parseInt((String) result.getText());
                Log.d(TAG, String.valueOf(hrReposP0));
                ((TextView) findViewById(R.id.reposTV)).setText("Squats dans 10s");
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        startSquats();
                    }
                }, 10000);
            }
        }.start();
    }

    public void startSquats(){
        setContentView(R.layout.squats_layout);
        SynchronizeAsyncTask mSynchronizeAsyncTask = new SynchronizeAsyncTask(this);
        mSynchronizeAsyncTask.execute(2);
        receiver = new MyReceiver(this);

        final TextView mTextView = findViewById(R.id.timerSquats);
        final TextView squatsTextView = findViewById(R.id.squatsCountTV);
        squatsTextView.setText("30 squats");

        new CountDownTimer(tempsSquats, tempsSquats /30) {
            int nbFlex = 30;
            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            public void onTick(long millisUntilFinished) {
                SynchronizeAsyncTaskTimer mSynchronizeAsyncTask = new SynchronizeAsyncTaskTimer(activity);
                mSynchronizeAsyncTask.execute((int) millisUntilFinished);
                int seconds = (int) ((millisUntilFinished /1000)%60);
                mTextView.setText("" + seconds + "s restantes" );
                v.vibrate(VibrationEffect.createOneShot(150,10));
                nbFlex --;
                squatsTextView.setText("" + nbFlex + " squats");
                SynchronizeAsyncTaskSteps mSynchronizeAsyncTask1 = new SynchronizeAsyncTaskSteps(activity);
                mSynchronizeAsyncTask1.execute(nbFlex);
            }

            public void onFinish() {
                TextView result = (TextView) findViewById(R.id.hrTV);
                squatsTextView.setText(result.getText() + " bpm");
                mTextView.setText("Stop !!");
                hrQuatsP1 = Integer.parseInt((String) result.getText());
                Log.d(TAG, String.valueOf(hrQuatsP1));
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        dernierRepos();
                    }
                }, 3000);
            }
        }.start();
    }


    public void dernierRepos(){
        setContentView(R.layout.pouls_repos_layout);
        ((FrameLayout) findViewById(R.id.background)).setBackgroundColor(Color.parseColor("#8c7ae6"));
        SynchronizeAsyncTask mSynchronizeAsyncTask = new SynchronizeAsyncTask(this);
        mSynchronizeAsyncTask.execute(3);

        final TextView mTextView = findViewById(R.id.timerReposTV);

        new CountDownTimer(tempsReposFinal, 100) {

            public void onTick(long millisUntilFinished) {
                SynchronizeAsyncTaskTimer mSynchronizeAsyncTask = new SynchronizeAsyncTaskTimer(activity);
                mSynchronizeAsyncTask.execute((int) millisUntilFinished);
                int minutes = (int) ((millisUntilFinished / 1000)/60);
                int seconds = (int) ((millisUntilFinished /1000)%60);
                int millis = (int) ((millisUntilFinished % 1000)/10);

                mTextView.setText("" + minutes + ":" + seconds + ":" + millis );
            }

            public void onFinish() {
                TextView result = (TextView) findViewById(R.id.hrTV);
                mTextView.setText(result.getText() + " bpm");
                hrRecupP2 = Integer.parseInt((String) result.getText());
                Log.d(TAG, String.valueOf(hrReposP0));
                ((TextView) findViewById(R.id.reposTV)).setText("resultats dans 10s");
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                       results();
                    }
                }, 3000);
            }
        }.start();
    }

    public void results(){
        setContentView(R.layout.result_layout);

        TextView irTextView = (TextView) findViewById(R.id.irtv);
        TextView idTextView = (TextView) findViewById(R.id.idtv);
        double ir = (hrReposP0+hrQuatsP1+hrRecupP2-200)/10.;
        double id = ((hrQuatsP1-70)+2*(hrRecupP2-hrReposP0))/10;
        SynchronizeAsyncTaskResults mSynchronizeAsyncTask = new SynchronizeAsyncTaskResults(this);
        mSynchronizeAsyncTask.execute(hrReposP0,hrQuatsP1,hrRecupP2);

        if (ir<=0){
            irTextView.setText(ir + " très bon");
            irTextView.setTextColor(Color.parseColor("#4cd137"));
        } else if (0>ir && ir <= 5){
            irTextView.setText(ir + " bon");
            irTextView.setTextColor(Color.parseColor("#44bd32"));
        }
        else if (5>ir && ir <= 10){
            irTextView.setText(ir + " moyen");
            irTextView.setTextColor(Color.parseColor("#e1b12c"));
        }if (10>ir && ir <= 15){
            irTextView.setText(ir + " insiffisant");
            irTextView.setTextColor(Color.parseColor("#c23616"));
        }else{
            irTextView.setText(ir + " bilan médical complémentaire\n" +
                    "nécessaire");
            irTextView.setTextColor(Color.parseColor("#e84118"));
        }

        if (id<=0){
            idTextView.setText(id + " excellent");
            idTextView.setTextColor(Color.parseColor("#4cd137"));
        }else if (id>0 && id<=2){
            idTextView.setText(id + " très bien");
            idTextView.setTextColor(Color.parseColor("#44bd32"));
        }else if (id>2 && id<=4){
            idTextView.setText(id + " bon");
            idTextView.setTextColor(Color.parseColor("#44bd32"));
        }else if (id>4 && id<=6){
            idTextView.setText(id + " moyen");
            idTextView.setTextColor(Color.parseColor("#fbc531"));
        }else if (id>6 && id<=8){
            idTextView.setText(id + " faible");
            idTextView.setTextColor(Color.parseColor("#e1b12c"));
        }else if (id>8 && id<=10){
            idTextView.setText(id + " très faible");
            idTextView.setTextColor(Color.parseColor("#c23616"));
        }else{
            idTextView.setText(id + " mauvaise adaptation à l'éffort");
            idTextView.setTextColor(Color.parseColor("#e84118"));
        }
        SynchronizeAsyncTask mSynchronizeAsyncTask1 = new SynchronizeAsyncTask(this);
        mSynchronizeAsyncTask1.execute(4);

        int idMedicin = 2;
        int idPatient = 0;
        int idTest = 1;

        /*refMessage.child(String.valueOf(idMedicin)).child("listePatient").child(String.valueOf(idPatient)).child("listeTest").child(String.valueOf(idTest));

        refMessage.child("date").setValue("2020-02-02");
        refMessage.child("iD").setValue(id);
        refMessage.child("iR").setValue(ir);
        refMessage.child("idTest").setValue(idTest);
        refMessage.child("p0").setValue(hrReposP0);
        refMessage.child("p1").setValue(hrQuatsP1);
        refMessage.child("p2").setValue(hrRecupP2);*/

    }

    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        Log.d(TAG, "onDataChanged" + dataEvents);
        for (DataEvent event : dataEvents) {
            if (event.getType() == DataEvent.TYPE_CHANGED) {
// DataItem changed
                DataItem item = event.getDataItem();
                if (item.getUri().getPath().compareTo("/mobileActions") == 0) {
                    DataMap dataMap = DataMapItem.fromDataItem(item).getDataMap();
                    int st = dataMap.getInt("action", 0);
                    poulsRepos();
                }
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        startService(hrServiceIntent);
        IntentFilter filter = new IntentFilter(WearHRService.HEART_RATE_MESSAGE);
        Wearable.getDataClient(this).addListener(this);
        //filter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(receiver, filter);
    }

    @Override
    protected void onPause() {
        Wearable.getDataClient(this).removeListener(this);
        super.onPause();
        stopService(hrServiceIntent);
        //unregisterReceiver(receiver);
    }


}






