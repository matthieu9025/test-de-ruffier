package fr.matthieu.jeannin.testruffier.utils;

import android.app.Activity;
import android.os.AsyncTask;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.wearable.DataClient;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

import fr.matthieu.jeannin.testruffier.WearHRService;

public class SynchronizeAsyncTaskTimer extends AsyncTask<Integer, Integer, Integer> {
    private DataClient mDataClient ;
    public SynchronizeAsyncTaskTimer(Activity mContext) {
        super();
        mDataClient = Wearable.getDataClient(mContext);
    }
    @Override
    protected Integer doInBackground(Integer... params) {
        int stepCount = params[0];
        PutDataMapRequest putDataMapReq = PutDataMapRequest.create("/time");
        putDataMapReq.getDataMap().putInt("timer" ,stepCount);
        PutDataRequest putDataReq = putDataMapReq.asPutDataRequest();
        putDataReq.setUrgent();
        Task<DataItem> putDataTask = mDataClient.putDataItem(putDataReq);
        return -1;
    }
}