package fr.matthieu.jeannin.testruffier.utils;

import android.app.Activity;
import android.os.AsyncTask;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.wearable.DataClient;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

public class SynchronizeAsyncTaskResults extends AsyncTask<Integer, Integer, Integer> {
    private DataClient mDataClient ;
    public SynchronizeAsyncTaskResults(Activity mContext) {
        super();
        mDataClient = Wearable.getDataClient(mContext);
    }
    @Override
    protected Integer doInBackground(Integer... params) {
        int p0 = params[0];
        int p1 = params[1];
        int p2 = params[2];

        PutDataMapRequest putDataMapReq = PutDataMapRequest.create("/result");
        putDataMapReq.getDataMap().putString("results" ,p0+";"+p1+";"+p2);
        PutDataRequest putDataReq = putDataMapReq.asPutDataRequest();
        putDataReq.setUrgent();
        Task<DataItem> putDataTask = mDataClient.putDataItem(putDataReq);
        return -1;
    }
}